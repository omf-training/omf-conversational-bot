﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder;
using System.Threading.Tasks;
using System.Threading;
using System;

namespace OMF_Conversational_Bot
{
    public class FeedbackDialog : ComponentDialog
    {
        private readonly IStatePropertyAccessor<UserProfile> _userProfileAccessor;
        private readonly IStatePropertyAccessor<ConversationData> _conversationDataAccessor;

        public FeedbackDialog(UserState userState, ConversationState conversationState) : base(nameof(FeedbackDialog)) {
            _userProfileAccessor = userState.CreateProperty<UserProfile>("UserProfile");
            _conversationDataAccessor = conversationState.CreateProperty<ConversationData>(nameof(ConversationData));

            var waterfallSteps = new WaterfallStep[]
            {
                GetfeedBackAsync,
                ConfirmFeedBackAsync,
            };

            // Add named dialogs to the DialogSet. These names are saved in the dialog state.
            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), waterfallSteps));
            
            AddDialog(new NumberPrompt<int>(nameof(NumberPrompt<int>), FeedBackValidatorAsync));

            // The initial child Dialog to run.
            InitialDialogId = nameof(WaterfallDialog);
        }

        private async Task<DialogTurnResult> ConfirmFeedBackAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            if ((int)stepContext.Result != 0)
            {
                // Get the current profile object from user state.Run Dialo
                var userProfile = await _userProfileAccessor.GetAsync(stepContext.Context, () => new UserProfile(), cancellationToken);
                string msg = $"Thank you for your rating of {stepContext.Result}!";
                userProfile.Rating = Convert.ToInt32(stepContext.Result);
                await stepContext.Context.SendActivityAsync(MessageFactory.Text(msg), cancellationToken);
            } else {
                await stepContext.Context.SendActivityAsync(MessageFactory.Text("Thank you."), cancellationToken);
            }
            
            //Retreive the conversation data using the accessor.
            var conversationData = await _conversationDataAccessor.GetAsync(stepContext.Context, () => new ConversationData());
            //Reset the DialogMode flag back to false to re-enable the echo bot.
            conversationData.DialogMode = false;
            
            return await stepContext.EndDialogAsync(cancellationToken: cancellationToken);

        }

        private static Task<bool> FeedBackValidatorAsync(PromptValidatorContext<int> promptContext, CancellationToken cancellationToken)
        {
            // This condition is our validation rule. You can also change the value at this point.
            return Task.FromResult(promptContext.Recognized.Succeeded && promptContext.Recognized.Value >= 0 && promptContext.Recognized.Value <= 5);
        }

        private async Task<DialogTurnResult>  GetfeedBackAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            string message = $"Please rate your experience with us today on a scale of 1 (Poor) to 5 (Excellent) (0 to skip).";
            var promptOptions = new PromptOptions
            {
                Prompt = MessageFactory.Text(message),
                RetryPrompt = MessageFactory.Text("Please input a number between 1 and 5."),
                
            };

            return await stepContext.PromptAsync(nameof(NumberPrompt<int>), promptOptions, cancellationToken);
        }
    }
}
