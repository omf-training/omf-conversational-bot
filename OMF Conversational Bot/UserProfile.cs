﻿using Microsoft.Bot.Schema;

namespace OMF_Conversational_Bot
{
    /// <summary>
    /// This is our application state. Just a regular serializable .NET class.
    /// </summary>
    public class UserProfile
    {
        public string PolicyNumber { get; set; }

        public string Asset { get; set; }

        public string Name { get; set; }

        public int Age { get; set; }

        public Attachment Picture { get; set; }

        public int Rating { get; set; }
    }
}
