﻿// Defines a state property used to track conversation data.
namespace OMF_Conversational_Bot
{
    public class ConversationData
    {
        // The time-stamp of the most recent incoming message.
        public string Timestamp { get; set; }

        // The ID of the user's channel.
        public string ChannelId { get; set; }

        // Track whether we are in dialog mode
        public bool DialogMode { get; set; } = false;
    }
}
