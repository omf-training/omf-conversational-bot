// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Azure.CognitiveServices.Language.LUIS.Runtime.Models;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Logging;

// This IBot implementation can run any type of Dialog. The use of type parameterization is to allows multiple different bots
// to be run at different endpoints within the same project. This can be achieved by defining distinct Controller types
// each with dependency on distinct IBot types, this way ASP Dependency Injection can glue everything together without ambiguity.
// The ConversationState is used by the Dialog system. The UserState isn't, however, it might have been used in a Dialog implementation,
// and the requirement is that all BotState objects are saved at the end of a turn.
namespace OMF_Conversational_Bot
{
    public class ConversationBot<T> : ActivityHandler where T : Dialog
    {
        protected readonly Dialog _dialog;
        protected readonly BotState _conversationState;
        protected readonly BotState _userState;
        private readonly IBotServices _botServices;
        protected readonly ILogger _logger;
 
        public ConversationBot(ConversationState conversationState, UserState userState, T dialog, IBotServices botServices, ILogger<ConversationBot<T>> logger)
        {
            _conversationState = conversationState;
            _userState = userState;
            _botServices = botServices;
            _dialog = dialog;
            _logger = logger;
        }

        protected override async Task OnMembersAddedAsync(IList<ChannelAccount> membersAdded, ITurnContext<IConversationUpdateActivity> turnContext, CancellationToken cancellationToken)
        {
            foreach (var member in membersAdded)
            {
                StringBuilder sb = new StringBuilder(System.String.Empty, 50);
                sb.Append("Hello and Welcome - ");
                if (member.Id != turnContext.Activity.Recipient.Id)
                {
                    sb.Append(member.Name);
                    await turnContext.SendActivityAsync(MessageFactory.Text(sb.ToString(), sb.ToString()), cancellationToken);
                }
            }
        }
        
        public override async Task OnTurnAsync(ITurnContext turnContext, CancellationToken cancellationToken = default)
        {
            await base.OnTurnAsync(turnContext, cancellationToken);

            // Save any state changes that might have occurred during the turn.
            await _conversationState.SaveChangesAsync(turnContext, false, cancellationToken);
            await _userState.SaveChangesAsync(turnContext, false, cancellationToken);
        }

        protected override async Task OnMessageActivityAsync(ITurnContext<IMessageActivity> turnContext, CancellationToken cancellationToken)
        {
            _logger.LogInformation("Running dialog with Message Activity.");

            // First, we use the dispatch model to determine which cognitive service (LUIS or QnA) to use.
            var recognizerResult = await _botServices.Recognizer.RecognizeAsync(turnContext, cancellationToken);
            
            // Top intent tell us which cognitive service to use.
            var topIntent = recognizerResult.GetTopScoringIntent();

            // Get the state properties from the turn context.
            var conversationStateAccessors = _conversationState.CreateProperty<ConversationData>(nameof(ConversationData));
            var conversationData = await conversationStateAccessors.GetAsync(turnContext, () => new ConversationData());
            if (conversationData.DialogMode)
            {
                //We are currently collecting claim details.
                await _dialog.RunAsync(turnContext, _conversationState.CreateProperty<DialogState>(nameof(DialogState)), cancellationToken);
            } else {
                //Dispatch to intent handlers
                if (topIntent.score >= 0.5)
                    await DispatchToTopIntentAsync(turnContext, topIntent.intent, recognizerResult, cancellationToken);
                else //The confidence for this intent is too low, run utterance against QnA
                    await DispatchToTopIntentAsync(turnContext, "QnA", recognizerResult, cancellationToken);
            }
        }

        private async Task DispatchToTopIntentAsync(ITurnContext<IMessageActivity> turnContext, string intent, RecognizerResult recognizerResult, CancellationToken cancellationToken)
        {
            switch (intent)
            {
                case "ClaimStart":
                    await ProcessClaimStartAsync(turnContext, recognizerResult.Properties["luisResult"] as LuisResult, cancellationToken);
                    break;
                case "SmallTalkBotIdentification":
                    await ProcessSmallTalkBotIdentificationAsync(turnContext, recognizerResult.Properties["luisResult"] as LuisResult, cancellationToken);
                    break;
                case "ThanksMessage":
                    await ProcessThanksMessageAsync(turnContext, recognizerResult.Properties["luisResult"] as LuisResult, cancellationToken);
                    break;
                case "GreetingMessage":
                    await ProcessGreetingMessageAsync(turnContext, recognizerResult.Properties["luisResult"] as LuisResult, cancellationToken);
                    break;
                case "ProductInfo":
                    await ProcessProductInfoAsyc(turnContext, recognizerResult.Properties["luisResult"] as LuisResult, cancellationToken);
                    break;
                case "HelpMe":
                    await ProcessHelpMeAsyc(turnContext, recognizerResult.Properties["luisResult"] as LuisResult, cancellationToken);
                    break;
                case "PrivacyPolicy":
                    await ProcessPrivacyPolicyAsyc(turnContext, recognizerResult.Properties["luisResult"] as LuisResult, cancellationToken);
                    break;
                case "None":
                    await ProcessNoneAsyc(turnContext, recognizerResult.Properties["luisResult"] as LuisResult, cancellationToken);
                    break;
                case "QnA":
                    await ProcessSampleQnAAsync(turnContext, cancellationToken);
                    break;
                default:
                    _logger.LogInformation($"Dispatch unrecognized intent: {intent}.");
                    await turnContext.SendActivityAsync(MessageFactory.Text($"Dispatch unrecognized intent: {intent}."), cancellationToken);
                    break;
            }
        }

        private async Task ProcessNoneAsyc(ITurnContext<IMessageActivity> turnContext, LuisResult luisResult, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        private async Task ProcessPrivacyPolicyAsyc(ITurnContext<IMessageActivity> turnContext, LuisResult luisResult, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        private async Task ProcessHelpMeAsyc(ITurnContext<IMessageActivity> turnContext, LuisResult luisResult, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        private async Task ProcessProductInfoAsyc(ITurnContext<IMessageActivity> turnContext, LuisResult luisResult, CancellationToken cancellationToken)
        {
            _logger.LogInformation("ProcessProductInfoAsyc");
            await ProcessSampleQnAAsync(turnContext, cancellationToken);
        }

        private async Task ProcessGreetingMessageAsync(ITurnContext<IMessageActivity> turnContext, LuisResult luisResult, CancellationToken cancellationToken)
        {
            _logger.LogInformation("ProcessGreetingMessageAsync");

            // Retrieve LUIS result for Bot Greeting.
            var topIntent = luisResult.TopScoringIntent.Intent;
            var greetingReply = MessageFactory.Text($"Hi there! I'm One Main Insurance's Virtual Assistant, here to help guide you to find the information you need!");
            greetingReply.SuggestedActions = new SuggestedActions()
            {
                Actions = new List<CardAction>()
                {
                    new CardAction() { Title = "Start a Claim", Type = ActionTypes.ImBack, Value = "I would like to start a claim", Image = "https://via.placeholder.com/50/FF0000?text=Start+Claim", ImageAltText = "R" },
                    new CardAction() { Title = "Continue a Claim", Type = ActionTypes.ImBack, Value = "I would like to continue a claim", Image = "https://via.placeholder.com/50/FFFF00?text=Continue+Claim", ImageAltText = "Y" },
                    new CardAction() { Title = "Check my Claim Status", Type = ActionTypes.ImBack, Value = "I would like to check my claim status", Image = "https://via.placeholder.com/50/0000FF?text=Check+Claim", ImageAltText = "B"   },
                },
            };
            await turnContext.SendActivityAsync(greetingReply, cancellationToken);
            //await turnContext.SendActivityAsync(MessageFactory.Text($"GreetingMessage top intent {topIntent}."), cancellationToken);
            //await turnContext.SendActivityAsync(MessageFactory.Text($"GreetingMessage intents detected:\n\n{string.Join("\n\n", luisResult.Intents.Select(i => i.Intent))}"), cancellationToken);
            /*if (luisResult.Entities.Count > 0)
            {
                await turnContext.SendActivityAsync(MessageFactory.Text($"GreetingMessage entities were found in the message:\n\n{string.Join("\n\n", luisResult.Entities.Select(i => i.Entity))}"), cancellationToken);
            }*/
        }

        private async Task ProcessThanksMessageAsync(ITurnContext<IMessageActivity> turnContext, LuisResult luisResult, CancellationToken cancellationToken)
        {
            var _userProfileAccessor = _userState.CreateProperty<UserProfile>("UserProfile");
            var userProfile = await _userProfileAccessor.GetAsync(turnContext, () => new UserProfile(), cancellationToken);
            if (!(userProfile.Name is null))
                await turnContext.SendActivityAsync(MessageFactory.Text($"You're welcome {userProfile.Name}!"), cancellationToken);
            else
                await turnContext.SendActivityAsync(MessageFactory.Text($"You're welcome!"), cancellationToken);
        }

        private async Task ProcessSmallTalkBotIdentificationAsync(ITurnContext<IMessageActivity> turnContext, LuisResult luisResult, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        private async Task ProcessClaimStartAsync(ITurnContext<IMessageActivity> turnContext, LuisResult luisResult, CancellationToken cancellationToken)
        {
            // Get the state properties from the turn context.
            var conversationStateAccessors = _conversationState.CreateProperty<ConversationData>(nameof(ConversationData));
            var conversationData = await conversationStateAccessors.GetAsync(turnContext, () => new ConversationData());
            conversationData.DialogMode = true;
            await _dialog.RunAsync(turnContext, _conversationState.CreateProperty<DialogState>(nameof(DialogState)), cancellationToken);
        }
        
        private async Task ProcessSampleQnAAsync(ITurnContext<IMessageActivity> turnContext, CancellationToken cancellationToken)
        {
            _logger.LogInformation("ProcessSampleQnAAsync");

            var results = await _botServices.SampleQnA.GetAnswersAsync(turnContext);
            if (results.Any())
            {
                await turnContext.SendActivityAsync(MessageFactory.Text(results.First().Answer), cancellationToken);
            }
            else
            {
                await turnContext.SendActivityAsync(MessageFactory.Text("Sorry, we did not understand your intent nor could not find an answer in the Q and A system. Please rephrase your question."), cancellationToken);
            }
        }
    
    }
}
