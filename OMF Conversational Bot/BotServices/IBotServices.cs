﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
//
// Generated with Bot Builder V4 SDK Template for Visual Studio EmptyBot v4.10.2

using Microsoft.Bot.Builder.AI.Luis;
using Microsoft.Bot.Builder.AI.QnA;

namespace OMF_Conversational_Bot
{
    public interface IBotServices
    {
        LuisRecognizer Recognizer { get; }

        QnAMaker SampleQnA { get; }
    }
}
